import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'
import NotFound from '@/views/NotFound'
import ElementUI from '@/views/ElementUI'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }, {
      path: '/login',
      name: 'Login',
      component: Login
    }, {
      path: '/NotFound',
      name: 'notFound',
      component: NotFound
    } ,{
      path: '/elementUI',
      name: 'ElementUI',
      component: ElementUI
    }
  ]
})
