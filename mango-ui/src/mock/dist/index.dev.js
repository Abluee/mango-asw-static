"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _mockjs = _interopRequireDefault(require("mockjs"));

var _global = require("@/utils/global");

var login = _interopRequireWildcard(require("./modules/login"));

var user = _interopRequireWildcard(require("./modules/user"));

var role = _interopRequireWildcard(require("./modules/role"));

var dept = _interopRequireWildcard(require("./modules/dept"));

var menu = _interopRequireWildcard(require("./modules/menu"));

var dict = _interopRequireWildcard(require("./modules/dict"));

var config = _interopRequireWildcard(require("./modules/config"));

var log = _interopRequireWildcard(require("./modules/log"));

var loginlog = _interopRequireWildcard(require("./modules/loginlog"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

// 1. 开启/关闭[所有模块]拦截, 通过调[openMock参数]设置.
// 2. 开启/关闭[业务模块]拦截, 通过调用fnCreate方法[isOpen参数]设置.
// 3. 开启/关闭[业务模块中某个请求]拦截, 通过函数返回对象中的[isOpen属性]设置.
var openMock = true; // let openMock = false

fnCreate(login, openMock);
fnCreate(user, openMock);
fnCreate(role, openMock);
fnCreate(dept, openMock);
fnCreate(menu, openMock);
fnCreate(dict, openMock);
fnCreate(config, openMock);
fnCreate(log, openMock);
fnCreate(loginlog, openMock);
/**
 * 创建mock模拟数据
 * @param {*} mod 模块
 * @param {*} isOpen 是否开启?
 */

function fnCreate(mod) {
  var isOpen = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

  if (isOpen) {
    for (var key in mod) {
      (function (res) {
        if (res.isOpen !== false) {
          var url = _global.baseUrl;

          if (!url.endsWith("/")) {
            url = url + "/";
          }

          url = url + res.url;

          _mockjs["default"].mock(new RegExp(url), res.type, function (opts) {
            opts['data'] = opts.body ? JSON.parse(opts.body) : null;
            delete opts.body;
            console.log('\n');
            console.log('%cmock拦截, 请求: ', 'color:blue', opts);
            console.log('%cmock拦截, 响应: ', 'color:blue', res.data);
            return res.data;
          });
        }
      })(mod[key]() || {});
    }
  }
}